package com.example.probe

class ArrayMaxFreq() {

    lateinit var arrayToCheck: Array<Any>
    var helpMap: MutableMap<Any,Int> = mutableMapOf()

    init {
        print(message = "\nFirst constructor: Built!")
    }

    constructor(newarray: Array<Any>):this(){
        print(message = "\nSecond constructor: Received an array: ")
        this.arrayToCheck = newarray

        for (i in this.arrayToCheck.indices){
            print(newarray[i])
            print(" ")
        }
        print("\n")
    }

    fun mostFreqElem():Any{
        for (i in this.arrayToCheck.indices){
            if (!helpMap.containsKey(arrayToCheck[i])){
                helpMap.put(arrayToCheck[i], 1)
            }
            else{
                val pom=helpMap.get(arrayToCheck[i])
                helpMap.set(arrayToCheck[i], if (pom !=null) pom+1 else 1 )
            }
        }
        var mostFreq = helpMap.maxBy {it.value}
        //return mostFreq!!.key
        return if (mostFreq != null) mostFreq.key else "Your array is empty!" //in case of an empty array returns a string


    }
}

fun main(args: Array<String>) {

    val exampleArray = ArrayMaxFreq(arrayOf("Ana",3,4,3,3, "Marija", "Ana", "Marko", "Ana", 3))
    val elem =exampleArray.mostFreqElem()
    print(elem)

    val exampleArray2 = ArrayMaxFreq(arrayOf())
    val elem2 =exampleArray2.mostFreqElem()
    print(elem2)

}